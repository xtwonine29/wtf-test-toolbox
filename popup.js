$(document).ready(function(){

    $("#status").html("Hello");

    // setting the saved/default values when popup is opened

    chrome.storage.sync.get(['wtf-saved-color', 'wtf-auto-paint'], function(items){
        // console.log("Saved: " + items['wtf-saved-color']);
        // console.log("Autopaint: " + items['wtf-auto-paint']);

        if(typeof items['wtf-saved-color'] !== 'undefined'){
            $('#color-name').val(items['wtf-saved-color']);
        }

        if(typeof items['wtf-auto-paint'] !== 'undefined'){
            $("#color-autoapply").attr("checked", true);
        }
    });

    // binding everything

    $("#color-name").on("change", function(e){
        chrome.storage.sync.set({'wtf-saved-color': $("#color-name").val()}, function() {
            // message('Color saved');
            $("#status").html('Color ' + $("#color-name").val() + ' selected');
            console.log("Saved: " + $("#color-name").val());
            // $("#color-save").attr("disabled", false);
        });
    });

    $("#color-autoapply").on("click", function(e){
                console.log("Autoapply state: " + ($("#color-autoapply").is(":checked")));
        if($("#color-autoapply").is(":checked")){
            chrome.storage.sync.set({'wtf-auto-paint': true}, function() {
                $("#status").html('Autopaint engaged');
            });
        }else{
            chrome.storage.sync.remove('wtf-auto-paint', function() {
                $("#status").html('Autopaint disengaged');
            });
        }
    });

    $("#color-submit").on("click", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#status").html("Painting...");

        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {type:"paintAllSections", color: $("#color-name").val()}, function(response) {
                $("#status").html(response);
            })
        })
    })
});

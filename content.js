var oldColorClasses = [
    "section-black",
    "section-blue",
    "section-cyan",
    "section-gray",
    "section-darkgray",
    "section-green",
    "section-media",
    "section-media-light",
    "section-mint",
    "section-orange",
    "section-pink",
    "section-purple",
    "section-red",
    "section-white", // ui02
    "section-black-blue",
    "section-black-red",
    "section-none",
    "section-white-blue",
    "section-white-red",
    "wsection-black",
    "wsection-black2",
    "wsection-darkgray",
    "wsection-darkgray2",
    "wsection-gray",
    "wsection-lightpink",
    "wsection-media",
    "wsection-media-inverse",
    "wsection-media-light",
    "wsection-white",
    "section-totalblack",
    "section-beige",
    "section-darkslateblue",
    "section-lightgray",
    "section-purple",
    "s-black",
    "s-gray",
    "s-media",
    "s-media-l",
    "s-white",
    "sc-w",
    "sc-b",
    "sc-d",
    "sc-m",
    "sc-ml",
    "sc-ml",
    "sc-blue",
    "sc-cyan",
    "sc-green",
    "sc-mint",
    "sc-orange",
    "sc-pink",
    "sc-purple",
    "sc-red",
    "sc-tblack",
    "sc-dgray",
    "sc-beige",
    "sc-gracier",
    "sc-glacier",
    "sc-purple"
];

var paintIt = function(color){


    setTimeout(function(){

        $(".section").removeClass(oldColorClasses.join(" ")).addClass(color);
        $(".section .section-bg-layer").attr("style", "");
        $(".section .section-bg-overlay").attr("class","").addClass("section-bg-layer section-bg-overlay")

        if(color === "sc-ml" || color === "section-media-light"){
            $("section .section-bg-layer").attr("style", "background: url('http://static.design.webnodev.com/files/1m/1mh/1mhb7k.png') repeat;");
        }
        if(color === "sc-m" || color === "section-media"){
            $("section .section-bg-layer").attr("style", "background: url('http://static.design.webnodev.com/files/0g/0gy/0gy8qo.png') repeat;");
        }

    }, 200);

};

chrome.runtime.onMessage.addListener(
    function(message, sender, sendResponse) {
        switch(message.type) {
            case "paintAllSections":
                paintIt(message.color);
                sendResponse("Done painting");
                break;
        }
    }
);

$(document).ready(function(){
    chrome.storage.sync.get(['wtf-saved-color', 'wtf-auto-paint'], function (items) {
        if(typeof items['wtf-auto-paint'] !== 'undefined') {
            paintIt(items['wtf-saved-color']);
        }
    })
});
